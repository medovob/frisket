import React from 'react';
import { shallow } from 'enzyme';

import App from './app';

describe('<App />', () => {
  it('Component to render children', () => {
    expect(shallow(<App>Hello</App>).contains("Hello")).toEqual(true);
  });
});
